# Arctos App

This is the repository for the Arctos iOS application.

### Status

Development on this app is just beginning: when shipped, the alpha Arctos Keys will include a basic Arctos App.

### Get Involved

If you would like to assist our team with building loyal digital agents for the crypto economy, please get in touch at https://arctoskey.com.
